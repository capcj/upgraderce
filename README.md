# UpdaterCE

A Image updater for the WinCE Handhelds in .NET Compact Framework 2.0

Compatibility with Motorola/Zebra/Symbol/Honeywell handhelds

## Steps:

- Change in the version.txt how your image will be called (ideally the handheld model);
- Put the UpdaterCEInstaller.bat in the Startup 
(Use StartUpCtl or Copy to your Windows\Startup folder after coldboot - it's a important step because
the UpdaterCE CANNOT RUN IN THE \APPLICATION FOLDER DUE THE FORMAT PROCESS);
- The UpdaterCE will be in the \Program Files\UpdaterCE path;

## What the UpdaterCE does?

- Formats \Application if has version zip image in the same locale
of the .exe;
- Unzips the version image in the Application Folder;
- Cold boots the handheld;

Due the nature of the project, the error/info messages are in portuguese (pt-br), I'm sorry.

