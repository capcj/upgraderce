@ECHO OFF
:: :: :: :: :: :: :: :: :: :: :: :: :: :: :: :: :: :: :: :: :: :: :: :: :: ::
::                                                                         ::
::                              UpdaterCEInstaller     					   ::
::      Made for installation of the UpdaterCE in the Windows storage      ::
::                                                                         ::
:: :: :: :: :: :: :: :: :: :: :: :: :: :: :: :: :: :: :: :: :: :: :: :: :: ::

MKDIR "\Program Files\UpdaterCE"
COPY \Application\UpdaterCE\* "\Program Files\UpdaterCE"