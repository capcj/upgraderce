﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Reflection;
using Ionic.Zip;
using System.Threading;

namespace UpdaterCE
{
    public partial class Form1 : Form
    {
        private string StartupPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase);
        private string Version;
        private string ImagePath;
        public Form1()
        {
            Version = GetHandheldVersion();
            ImagePath = StartupPath + @"\" + Version + ".zip";
            InitializeComponent();
            label1.Text += " " + Version;
        }

        private void DeleteFiles(string directory)
        {
            DirectoryInfo di = new DirectoryInfo(directory);

            foreach (FileInfo file in di.GetFiles())
            {
                try
                {
                    if (file.ToString() != "Otl.exe")
                    {
                        file.Delete();
                    }
                }
                catch
                {
                    MessageBox.Show("Não foi possível deletar, cheque permissões: " + file.FullName);
                }
            }
            foreach (DirectoryInfo dir in di.GetDirectories())
            {
                if (dir.ToString() != "GPD" && dir.ToString() != "System" && dir.ToString() != "Samples.C")
                {
                    try
                    {
                        DeleteFiles(dir.FullName);
                        dir.Delete();
                    }
                    catch
                    {
                        continue;
                    }
                }
            }
        }

        private string GetHandheldVersion()
        {
            if (!File.Exists(StartupPath + @"\version.txt"))
            {
                throw new Exception(
                    "Erro: Arquivo de versão com o modelo do coletor deve estar na mesma pasta do Atualizador"
                );
            }
            StringBuilder sb = new StringBuilder();
            using (StreamReader sr = new StreamReader(StartupPath + @"\version.txt"))
            {
                String line;
                while ((line = sr.ReadLine()) != null)
                {
                    sb.Append(line);
                }
            }
            return sb.ToString();
        }

        private void UnzipImage()
        {
            try
            {
                using (var zip1 = Ionic.Zip.ZipFile.Read(ImagePath))
                {
                    foreach (var entry in zip1)
                    {
                        entry.Extract(@"\Application", ExtractExistingFileAction.OverwriteSilently);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Aconteceu algo inesperado! Erro: " + ex);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (File.Exists(ImagePath))
            {
                DeleteFiles(@"\Application");
                UnzipImage();
                RebootCE.HardReset();
            }
            else
            {
                MessageBox.Show("Imagem " + Version + ".zip está faltando no diretório do Atualizador");
            }
        }
    }
}